import graphene
from graphene_django.types import DjangoObjectType

from .models import Project
from users.schema import UserType

class ProjectType(DjangoObjectType):
    class Meta:
        model = Project

class CreateProject(graphene.Mutation):
    id = graphene.String()
    name = graphene.String()
    description = graphene.String()
    owner = graphene.Field(UserType)

    class Arguments:
        name = graphene.String()
        description = graphene.String()

    def mutate(self, info, name, description):
        user = info.context.user
        id = user.id or None
        if id is None:
            raise Exception('User not log in!')
        project = Project(name=name, description=description, owner=user)
        project.save()
        return CreateProject(
            id=project.id,
            name=project.name,
            description=project.description,
            owner=user
        )

class ProjectMutation(graphene.ObjectType):
    create_project = CreateProject.Field()
