from django.db import models
from django.conf import settings

class Project(models.Model):
    name = models.CharField(max_length=16)
    description = models.TextField()
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, on_delete=models.CASCADE)

    def __repr__(self):
        return self.name
